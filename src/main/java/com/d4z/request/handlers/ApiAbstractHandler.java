package com.d4z.request.handlers;

import io.vertx.core.json.JsonObject;

public abstract class ApiAbstractHandler {
	public JsonObject cookie;
	public JsonObject session;
	public String method;
	public JsonObject request;
	public JsonObject response;

	public JsonObject getCookie() {
		return cookie;
	}

	public void setCookie(JsonObject cookie) {
		this.cookie = cookie;
	}

	public JsonObject getSession() {
		return session;
	}

	public void setSession(JsonObject session) {
		this.session = session;
	}

	public String getMethod() {
		return method;
	}

	public void setMethod(String method) {
		this.method = method;
	}

	public JsonObject getRequest() {
		return request;
	}

	public void setRequest(JsonObject request) {
		this.request = request;
	}

	public JsonObject getResponse() {
		return response;
	}

	public void setResponse(JsonObject response) {
		this.response = response;
	}
}
