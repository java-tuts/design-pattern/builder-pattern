package com.d4z.request.builders;

import java.lang.reflect.Constructor;

import com.d4z.request.handlers.ApiAbstractHandler;

import io.vertx.core.json.JsonObject;

public class PostActionHandlerBuilder implements IActionHandlerBuilder {
	private ApiAbstractHandler handler;
	
	public PostActionHandlerBuilder(String pClassHandler) throws Exception {
		Class<?> myClass = Class.forName(pClassHandler);
		Constructor<?> constructor = myClass.getConstructor();
		handler = (ApiAbstractHandler) constructor.newInstance();
	}
	
	@Override
	public ApiAbstractHandler build() {
		return handler;
	}
	
	@Override
	public IActionHandlerBuilder setCookie(JsonObject pCookie) {
		handler.setCookie(pCookie);
		return this;
	}
	
	@Override
	public IActionHandlerBuilder setMethod(String pMethod) {
		handler.setMethod(pMethod);
		return this;
	}
	
	@Override
	public IActionHandlerBuilder setSession(JsonObject pSession) {
		handler.setSession(pSession);
		return this;
	}
	
	@Override
	public <R> IActionHandlerBuilder setRequest(R pRequest) {
		if (pRequest instanceof JsonObject) {
			handler.setRequest((JsonObject) pRequest);
		}
		return this;
	}
}
